module aja/go-mongo

go 1.15

require (
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.3
	github.com/go-playground/validator v9.31.0+incompatible
	github.com/google/uuid v1.1.2
	github.com/stretchr/testify v1.6.1
	go.mongodb.org/mongo-driver v1.4.2
)
