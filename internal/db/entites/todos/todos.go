package todos

import (
	"aja/go-mongo/internal/db"
	"context"
	"time"

	"github.com/go-playground/validator"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

//Todo is the struct representing the database entity
type Todo struct {
	ID        string    `json:"id" `
	Title     string    `json:"title" validate:"required"`
	Body      string    `json:"body" validate:"required"`
	Completed string    `json:"completed" validate:"required"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

//Validate validates the struct for testing
func (todo *Todo) Validate() error {
	validate := validator.New()
	return validate.Struct(todo)
}

//Save saves the todo in the database
func (todo *Todo) Save() (string, error) {
	collection := db.DB.Database("go_mongo").Collection("todos")
	insertResult, err := collection.InsertOne(context.TODO(), todo)

	if err != nil {
		return "", err
	}

	return insertResult.InsertedID.(primitive.ObjectID).Hex(), nil

}

//EditTodo updates the collection with the new information in the Todo struct
func (todo *Todo) EditTodo() error {
	collection := db.DB.Database("go_mongo").Collection("todos")
	update := bson.M{
		"$set": bson.M{
			"title":      todo.Title,
			"body":       todo.Body,
			"completed":  todo.Completed,
			"updated_at": time.Now(),
		},
	}

	_, err := collection.UpdateOne(context.TODO(), bson.M{"id": todo.ID}, update)
	if err != nil {
		return err
	}

	return nil
}

//DeleteTodo deletes the todo from the database
func (todo *Todo) DeleteTodo() error {
	collection := db.DB.Database("go_mongo").Collection("todos")
	_, err := collection.DeleteOne(context.TODO(), bson.M{"id": todo.ID})
	if err != nil {
		return err
	}

	return nil
}

//GetAllTodos returns all todos in collection
func GetAllTodos() ([]Todo, error) {
	collection := db.DB.Database("go_mongo").Collection("todos")
	findOptions := options.Find()

	var results []Todo

	cur, err := collection.Find(context.TODO(), bson.D{{}}, findOptions)
	if err != nil {
		return []Todo{}, err
	}

	for cur.Next(context.TODO()) {
		var elem Todo
		err := cur.Decode(&elem)
		if err != nil {
			return []Todo{}, err
		}

		results = append(results, elem)
	}

	if err := cur.Err(); err != nil {
		return []Todo{}, err
	}

	cur.Close(context.TODO())
	return results, nil
}

//FindWithID returns the todo with the specified ID
func FindWithID(id string) (Todo, error) {
	filter := bson.M{"id": id}
	collection := db.DB.Database("go_mongo").Collection("todos")

	var todo Todo
	err := collection.FindOne(context.TODO(), filter).Decode(&todo)
	if err != nil {
		return Todo{}, err
	}

	return todo, nil
}

//GetFirstElementOfCollection returns the first element in the todos collection
func GetFirstElementOfCollection() (Todo, error) {
	collection := db.DB.Database("go_mongo").Collection("todos")
	findOptions := options.Find()

	cur, err := collection.Find(context.TODO(), bson.D{{}}, findOptions)
	if err != nil {
		return Todo{}, err
	}

	for cur.Next(context.TODO()) {
		var elem Todo
		err := cur.Decode(&elem)
		if err != nil {
			return Todo{}, err
		}

		return elem, nil
	}

	return Todo{}, nil
}
