package db

import (
	"context"
	"log"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

//DB is the client connection to the database
var DB *mongo.Client

//ConnectToDB initializes the DB variable with an open connection URI
func ConnectToDB() {
	clientOptions := options.Client().ApplyURI("mongodb://root:root@3.124.3.205:27017")
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatal(err)
	}

	DB = client
}
