package todos_test

import (
	"aja/go-mongo/internal/db/entites/todos"
	"aja/go-mongo/internal/router"
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
)

type AllTodosResponse struct {
	Status  int           `validate:"required"`
	Message string        `validate:"required"`
	Data    []*todos.Todo `validate:"required"`
}

type SingleTodoResponse struct {
	Status  int         `validate:"required"`
	Message string      `validate:"required"`
	Data    *todos.Todo `validate:"required"`
}

type CreateTodoResponse struct {
	Status   int    `validate:"required"`
	Message  string `validate:"required"`
	Inserted string `validate:"required"`
}

type NoDataMessage struct {
	Status  int    `validate:"required"`
	Message string `validate:"required"`
}

func TestGetAllTodos(t *testing.T) {
	testRouter := router.SetupRouter()
	var todos AllTodosResponse
	req, err := http.NewRequest("GET", "/todos", nil)
	assert.Nil(t, err)

	response := httptest.NewRecorder()
	wg := &sync.WaitGroup{}
	wg.Add(1)

	go func() {
		defer wg.Done()
		testRouter.ServeHTTP(response, req)
		err = json.Unmarshal(response.Body.Bytes(), &todos)
		assert.Nil(t, err)

		if todos.Data != nil {
			for _, todo := range todos.Data {
				assert.Nil(t, todo.Validate())
			}
		}
	}()

	wg.Wait()
}

func TestGetSingleTodo(t *testing.T) {
	testRouter := router.SetupRouter()
	sampleTodo, err := todos.GetFirstElementOfCollection()
	assert.Nil(t, err)
	wg := &sync.WaitGroup{}
	wg.Add(1)

	if sampleTodo.ID != "" {
		var expectedResponse SingleTodoResponse
		req, err := http.NewRequest("GET", "/todo/"+sampleTodo.ID, nil)
		response := httptest.NewRecorder()
		go func() {
			defer wg.Done()
			testRouter.ServeHTTP(response, req)
			stringBody := string(response.Body.Bytes())
			fmt.Println(stringBody)
			err = json.Unmarshal(response.Body.Bytes(), &expectedResponse)
			assert.Nil(t, err)
			assert.Nil(t, expectedResponse.Data.Validate())
		}()
	} else {
		var expectedResponse NoDataMessage
		req, err := http.NewRequest("GET", "/todo/test", nil)
		response := httptest.NewRecorder()
		go func() {
			defer wg.Done()
			testRouter.ServeHTTP(response, req)
			stringBody := string(response.Body.Bytes())
			fmt.Println(stringBody)
			err = json.Unmarshal(response.Body.Bytes(), &expectedResponse)
			assert.Nil(t, err)
		}()
	}

	wg.Wait()
}

func TestCreateTodo(t *testing.T) {
	var expectedResponse CreateTodoResponse

	testRouter := router.SetupRouter()
	sampleTodo, err := todos.GetFirstElementOfCollection()
	assert.Nil(t, err)
	sentJson, _ := json.Marshal(sampleTodo)

	req, err := http.NewRequest("POST", "/todo", bytes.NewBuffer(sentJson))
	assert.Nil(t, err)
	wg := &sync.WaitGroup{}
	wg.Add(1)
	go func() {
		defer wg.Done()
		req.Header.Set("Content-Type", "application/json")
		response := httptest.NewRecorder()
		testRouter.ServeHTTP(response, req)
		err = json.Unmarshal(response.Body.Bytes(), &expectedResponse)
		assert.Nil(t, err)
		assert.Equal(t, 201, expectedResponse.Status)
		sampleTodo.ID = expectedResponse.Inserted
		sampleTodo.DeleteTodo()
	}()
	wg.Wait()
}

func TestEditTodo(t *testing.T) {
	testRouter := router.SetupRouter()
	sampleTodo, err := todos.GetFirstElementOfCollection()
	assert.Nil(t, err)
	sentJson, _ := json.Marshal(sampleTodo)
	wg := &sync.WaitGroup{}
	wg.Add(1)

	if sampleTodo.ID != "" {
		var expectedResponse SingleTodoResponse
		req, err := http.NewRequest("PUT", "/todo/"+sampleTodo.ID, bytes.NewBuffer(sentJson))
		response := httptest.NewRecorder()
		go func() {
			testRouter.ServeHTTP(response, req)
			stringBody := string(response.Body.Bytes())
			fmt.Println(stringBody)
			err = json.Unmarshal(response.Body.Bytes(), &expectedResponse)
			assert.Nil(t, err)
			assert.Equal(t, http.StatusOK, expectedResponse.Status)
		}()
		wg.Done()
	} else {
		var expectedResponse NoDataMessage
		req, err := http.NewRequest("GET", "/todo/test", nil)
		response := httptest.NewRecorder()
		go func() {
			testRouter.ServeHTTP(response, req)
			stringBody := string(response.Body.Bytes())
			fmt.Println(stringBody)
			err = json.Unmarshal(response.Body.Bytes(), &expectedResponse)
			assert.Nil(t, err)
			assert.Equal(t, http.StatusOK, expectedResponse.Status)
		}()
		wg.Done()
	}

	wg.Wait()
}

func TestDeleteTodo(t *testing.T) {
	testRouter := router.SetupRouter()
	sampleTodo, err := todos.GetFirstElementOfCollection()
	assert.Nil(t, err)
	wg := &sync.WaitGroup{}
	wg.Add(1)

	if sampleTodo.ID != "" {
		var expectedResponse NoDataMessage
		req, err := http.NewRequest("DELETE", "/todo/"+sampleTodo.ID, nil)
		response := httptest.NewRecorder()
		testRouter.ServeHTTP(response, req)
		go func() {
			testRouter.ServeHTTP(response, req)
			stringBody := string(response.Body.Bytes())
			fmt.Println(stringBody)
			err = json.Unmarshal(response.Body.Bytes(), &expectedResponse)
			assert.Nil(t, err)
			assert.Equal(t, http.StatusOK, expectedResponse.Status)
			sampleTodo.Save()
		}()
		wg.Done()
	} else {
		var expectedResponse NoDataMessage
		req, err := http.NewRequest("DELETE", "/todo/test", nil)
		response := httptest.NewRecorder()
		testRouter.ServeHTTP(response, req)
		go func() {
			testRouter.ServeHTTP(response, req)
			stringBody := string(response.Body.Bytes())
			fmt.Println(stringBody)
			err = json.Unmarshal(response.Body.Bytes(), &expectedResponse)
			assert.Nil(t, err)
			assert.Equal(t, http.StatusOK, expectedResponse.Status)
			sampleTodo.Save()
		}()
		wg.Done()
	}
}
