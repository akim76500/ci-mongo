package todos

import (
	"aja/go-mongo/internal/db/entites/todos"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	guuid "github.com/google/uuid"
)

//GetAllTodos returns all todos in collection
func GetAllTodos(c *gin.Context) {
	result, err := todos.GetAllTodos()
	if err != nil {
		log.Printf("Error while getting all todos, Reason: %v\n", err)
		c.JSON(http.StatusInternalServerError, gin.H{
			"status":  http.StatusInternalServerError,
			"message": "Something went wrong",
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"status":  http.StatusOK,
		"message": "All Todos",
		"data":    result,
	})
	return
}

//GetSingleTodo returns a single todo identified by it's ID
func GetSingleTodo(c *gin.Context) {
	todo, err := todos.FindWithID(c.Param("todoId"))
	if err != nil {
		log.Printf("Error while getting a single todo, Reason: %v\n", err)
		c.JSON(http.StatusNotFound, gin.H{
			"status":  http.StatusNotFound,
			"message": "Todo not found",
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"status":  http.StatusOK,
		"message": "Single Todo",
		"data":    todo,
	})
	return
}

//CreateTodo creates a new todo based on the JSON sent
func CreateTodo(c *gin.Context) {
	var todo todos.Todo
	c.BindJSON(&todo)
	todo.ID = guuid.New().String()
	now := time.Now()
	todo.CreatedAt = now
	todo.UpdatedAt = now

	_, err := todo.Save()

	if err != nil {
		log.Printf("Error while inserting new todo into db, Reason: %v\n", err)
		c.JSON(http.StatusInternalServerError, gin.H{
			"status":  http.StatusInternalServerError,
			"message": "Something went wrong",
		})
		return
	}

	c.JSON(http.StatusCreated, gin.H{
		"status":   http.StatusCreated,
		"message":  "Todo created Successfully",
		"inserted": todo.ID,
	})
	return
}

//EditTodo modifies the identified todo with the new data passed in JSON
func EditTodo(c *gin.Context) {
	todoID := c.Param("todoId")

	var newTodo todos.Todo
	oldTodo, err := todos.FindWithID(todoID)
	if err != nil {
		log.Printf("Error, Reason: %v\n", err)
		c.JSON(http.StatusInternalServerError, gin.H{
			"status":  500,
			"message": "Something went wrong",
		})
		return
	}
	c.BindJSON(newTodo)

	if newTodo.Completed != "" {
		oldTodo.Completed = newTodo.Completed
	}
	if newTodo.Title != "" {
		oldTodo.Title = newTodo.Title
	}
	if newTodo.Body != "" {
		oldTodo.Body = newTodo.Body
	}

	err = oldTodo.EditTodo()
	if err != nil {
		log.Printf("Error, Reason: %v\n", err)
		c.JSON(http.StatusInternalServerError, gin.H{
			"status":  500,
			"message": "Something went wrong",
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"status":  200,
		"message": "Todo Edited Successfully",
	})
}

//DeleteTodo deletes the identified todo in collection
func DeleteTodo(c *gin.Context) {
	todoID := c.Param("todoId")
	todo, err := todos.FindWithID(todoID)
	if err != nil {
		log.Printf("Error while deleting a single todo, Reason: %v\n", err)
		c.JSON(http.StatusInternalServerError, gin.H{
			"status":  http.StatusInternalServerError,
			"message": "Something went wrong",
		})
		return
	}

	err = todo.DeleteTodo()
	if err != nil {
		log.Printf("Error while deleting a single todo, Reason: %v\n", err)
		c.JSON(http.StatusInternalServerError, gin.H{
			"status":  http.StatusInternalServerError,
			"message": "Something went wrong",
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"status":  http.StatusOK,
		"message": "Todo deleted successfully",
	})
	return
}
