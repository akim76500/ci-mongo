package router

import (
	todos_controller "aja/go-mongo/internal/controllers/todo"
	"aja/go-mongo/internal/db"
	"net/http"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

//Routes set ups the API routes
func Routes(router *gin.Engine) {
	router.GET("/", welcome)

	router.GET("/todos", todos_controller.GetAllTodos)
	router.GET("/todo/:todoId", todos_controller.GetSingleTodo)

	router.POST("/todo", todos_controller.CreateTodo)
	router.PUT("/todo/:todoId", todos_controller.EditTodo)
	router.DELETE("/todo/:todoId", todos_controller.DeleteTodo)

	router.NoRoute(notFound)
}

//SetupRouter sets up and returns a gin router
func SetupRouter() *gin.Engine {
	router := gin.Default()
	db.ConnectToDB()

	router.Use(cors.New(cors.Config{
		AllowAllOrigins: true,
		AllowMethods:    []string{"GET", "PUT", "POST", "DELETE"},
	}))

	// Route Handlers / Endpoints
	Routes(router)
	return router
}

func welcome(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"status":  200,
		"message": "Welcome To API",
	})
	return
}

func notFound(c *gin.Context) {
	c.JSON(http.StatusNotFound, gin.H{
		"status":  404,
		"message": "Route Not Found",
	})
	return
}
