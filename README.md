# go_mongo

## CI /CD

L'utilisation d'un CI/CD nous permet de valider que les modifications apportées au code ne compromettent pas le fonctionnement de l'application.
Nous avons fait le choix d'utiliser Gitlab, car c'était nous avions déjà de l'expérience avec son système de CI. De plus il reste gratuit en dessous de 400 CI/CD minutes, ce qui correspond parfaitement à une petite équipe.

*Choix Technologique :* **GitLab CI**
*Prix :* **Gratuit**

## Hebergement Serveur
Pour héberger notre serveur notre choix c'est porté sur AWS.
Etant l'une des plateformes les plus populaires, sa communauté nous as aider a mettre en place notre serveur et a répondre a nos question lors de mauvais fonctionnement. La solution n'ayant pas de besoin spécifique et n'étant pas très ambitieuse dans sa complexité, les solutions les plus écoonomes de AWS repondaient parfaitement a notre besoin.

*Choix Technologique :* **AWS**
*Prix :* **5$ / mois**

## Hebergement Base De Données
Pour héberger notre base de données nous avons fait le choix d'utiliser MongoAtlas, la plateforme officielle de mongodb.
En utilisant un cluster M2 nous pouvons assurer la connexion avec AWS, est étant la solution officielle pour ce SGBD son choix nous a paru logique.

*Choix Technologique :* **Mongo Atlas**
*Prix :* **9$ / mois**

## Limitations

Pour toutes les solutions selectionnées nous avons pris les premiers prix. Tous répondent parfaitement a nos besoin pour l'instant.
Cependant si jamais l'application venait a grandir en utilisateurs et ambitions, la base de données serait la première a devenir insuffisante.
Cependant Mongo Atlas permet des upgrades de ses clusters sans pertes de données. De même si jamais le serveur AWS venait a ne plus supporter les taches nécéssaire au foncitonnemet de l'application, grace au CI de gitlab, la migration pourrait se faire sans aucun downtime.

# Prix total : 14$ / mois
