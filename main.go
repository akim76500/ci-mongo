package main

import (
	"aja/go-mongo/internal/db/entites/todos"
	"aja/go-mongo/internal/router"
	"fmt"
	"log"
)

func main() {
	apiRouter := router.SetupRouter()
	todo, err := todos.GetAllTodos()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("%v", todo)
	log.Fatal(apiRouter.Run(":4747"))
}
